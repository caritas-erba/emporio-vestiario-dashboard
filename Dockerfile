FROM python:3.8

RUN mkdir emporio-vestiario-dashboard

COPY src/* emporio-vestiario-dashboard/
COPY requirements.txt emporio-vestiario-dashboard/

WORKDIR emporio-vestiario-dashboard
RUN pip install -r requirements.txt

EXPOSE 8501

CMD streamlit run app.py