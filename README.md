# Siloe Dashboard

Siloe è una dashboard pensata er l'esplorazione dei dati registrati tramite il software [Zaccheo](https://gitlab.com/caritas-erba/zaccheo-crm).

Siloe permette di:
- visualizzare e scaricare liste complete o filtrate dei dati di:
  - anagrafica degli utenti
  - tessere
  - prodotti
- visualizzare alcuni grafici riassuntivi dei dati
- visualizzare alcune statistiche riassuntive dei dati

## Installazione

È possibile scaricare Siloe tramite la pagina delle [release](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/releases). Una volta sulla pagina, scaricare l'installer associato alla versione più recente.

Una volta scaricato l'installer sul pc, eseguirlo e seguire la procedura guidata.

### Variabili d'ambiente

Siloe necessita di configurare alcune variabili d'ambiente per funzionare correttamente.

Per poter configurare le variabili d'ambiente, eseguire i seguenti passi contenuti in questa [guida](https://nonsoloprogrammi.net/variabili-dambiente-windows) o in altre analoghe che si possono trovare sul web.

Le nuove variabili d'ambiente da creare sono:
1. `SILOE_DB_FILE_PATH`: percorso al file `caritas-db.sql` creato da Zaccheo.
2. `SILOE_TEMP_PDF_DIR`: percorso ad una cartella (a piacimento) dove saranno temporaneamente salvati i pdf relativi alle viste da esportare delle tabelle.
3. `SILOE_EMPORIO_LOCATION`: nome della località (comune) dove è situato l'emporio. Questa informazione è utilizzata per visualizzare nei grafici il numero degli utenti che hanno residenza nello stesso comune dove è situato l'emporio.

### Manuale utente
Siloe è provvisto di un manuale utente che illustra il funzionamento del programma e le sue funzionalità. È possibile scaricarlo dalla stessa [pagina](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/releases) in cui è presente l'installer. Una volta scaricato lo zip, estrarlo in una cartella a propria scelta. Per visualizzare il manuale, entrare nella cartella estratta e aprire il file (con un doppio click) *index.html*.

## Sviluppo

### Esecuzione
Lo sviluppo può essere eseguito anche su Linux.

È necessario configurare le variabili d'ambiente indicate precedentemente.

Per avviare l'app, lanciare il comando:
```
streamlit run src/app.py
```

### Versionamento
Il versionamento del codice è gestito automaticamente tramite il pacchetto `semantic-release`. La versione associata all'app, visualizzata in fase di installazione, è definita nella variabile `version` del file `installer.cfg`. Attualmente non è possibile aggiornare automaticamente questa versione, è necessario quindi farlo manualmente.
