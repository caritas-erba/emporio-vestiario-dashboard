import streamlit as st
import plotly.graph_objects as go

# import plotly.io as pio
#
# # TODO mmm maybe not here. Does it really work?
# pio.templates.default = "plotly"


def create_plotly_figure(data: dict, layout: dict):
    return go.Figure(data=data, layout=layout)


# TODO put outside st.plotly_chart to make possible to use plot in columns (and put filters by side)
def create_chart(data: list, layout: dict):
    st.plotly_chart(go.Figure(data=data, layout=layout), use_container_width=True)
