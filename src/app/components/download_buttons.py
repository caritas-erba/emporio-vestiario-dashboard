import streamlit as st
import pandas as pd

from constants import OUT_PDF_ALL_TABLE_FILE, OUT_PDF_FILTERED_TABLE_FILE
from app.helpers.pdf import create_table


def download_buttons(
    df: pd.DataFrame,
    filtered_df: pd.DataFrame,
    column_width: list,
    download_filename: str,
    paper_orientation: str = "L",
):
    # Saving temporary tables
    create_table(
        df,
        5,
        column_width,
        output_filename=OUT_PDF_ALL_TABLE_FILE,
        orientation=paper_orientation,
    )
    create_table(
        filtered_df,
        5,
        column_width,
        output_filename=OUT_PDF_FILTERED_TABLE_FILE,
        orientation=paper_orientation,
    )

    if not download_filename.endswith(".pdf"):
        download_filename = download_filename + ".pdf"

    # Download buttons
    left, right = st.columns(2)
    with open(OUT_PDF_ALL_TABLE_FILE, "rb") as pdf:
        left.download_button("Scarica tabella completa", pdf, download_filename)
    with open(OUT_PDF_FILTERED_TABLE_FILE, "rb") as pdf:
        right.download_button("Scarica tabella visualizzata", pdf, download_filename)
