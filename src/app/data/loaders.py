from datetime import datetime

import streamlit as st

from db.db_manager import DbManager
from db.queries import (
    ANAGRAFICA_ALL_QUERY,
    CARDS_ALL_QUERY,
    PRODUCTS_ALL_QUERY,
    TICKETS_ALL_QUERY,
)
from constants import DATE_DEFAULT_FORMAT
from data.headers.anagraphic_header import AnagraphicHeader as ah
from data.headers.cards_header import CardsHeader as ch
from data.headers.products_header import ProductsHeader as ph
from data.headers.ticket_header import TicketsHeader as th


@st.experimental_memo
def load_anagrafica(as_dataframe=False):
    data = DbManager.query(ANAGRAFICA_ALL_QUERY, to_df=as_dataframe)
    if as_dataframe:
        data = data.rename(columns={i: col for i, col in enumerate(ah.all)})
        data[ah.family_id] = data[ah.family_id].astype(int)
    return data


@st.experimental_memo
def load_cards(as_dataframe=False):
    data = DbManager.query(CARDS_ALL_QUERY, to_df=as_dataframe)
    if as_dataframe:
        data = data.rename(columns={i: col for i, col in enumerate(ch.all)})
        data[ch.card_id] = data[ch.card_id].astype(int)
    return data


@st.experimental_memo
def load_products(as_dataframe=False):
    data = DbManager.query(PRODUCTS_ALL_QUERY, to_df=as_dataframe)
    if as_dataframe:
        data = data.rename(columns={i: col for i, col in enumerate(ph.all)})
    return data


@st.experimental_memo
def load_tickets(as_dataframe=False):
    data = DbManager.query(TICKETS_ALL_QUERY, to_df=as_dataframe)
    if as_dataframe:
        data = data.rename(columns={i: col for i, col in enumerate(th.all)})
        data[th.date] = data[th.date].apply(
            lambda x: datetime.strptime(x, DATE_DEFAULT_FORMAT)
        )
    return data
