import streamlit as st

from data.headers.anagraphic_header import AnagraphicHeader as ah
from data.headers.cards_header import CardsHeader as ch
from data.headers.products_header import ProductsHeader as ph


@st.experimental_memo
def beautify_anagrafica_table(data):
    data = data.rename(
        columns={col: new_col for col, new_col in zip(ah.all, ah.beauty_all)}
    )
    return data


@st.experimental_memo
def beautify_cards_table(data):
    data = data.rename(
        columns={col: new_col for col, new_col in zip(ch.all, ch.beauty_all)}
    )
    return data


@st.experimental_memo
def beautify_products_table(data):
    data = data.rename(
        columns={col: new_col for col, new_col in zip(ph.all, ph.beauty_all)}
    )
    return data
