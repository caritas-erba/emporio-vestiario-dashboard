import os

import fpdf.fpdf
from fpdf import FPDF
import pandas as pd

from constants import OUT_PDF_TABLES_PATH


def create_table_formtting(df: pd.DataFrame, line_height: int, column_width: list):
    """
    Creates the formatting needed to produce a pdf table.

    :param df:              Pandas dataframe
    :param line_height:     Line height
    :param column_width:    Column width. Must be in the same order as df.columns
    :return:                Formatting dict
    """
    formatting = {
        "line_height": line_height,
        "columns": [
            {"name": name.upper().replace("_", " "), "width": width}
            for name, width in zip(df.columns, column_width)
        ],
    }
    return formatting


def render_table_header(pdf: fpdf.fpdf.FPDF, formatting: dict, font_size: int = 14):
    """
    Creates table header.

    :param pdf:         FPDF file where render the table
    :param formatting:  definition of table format. Must the following keys:
        - line_height
        - columns: list of dictionaries containing each: column name and column width
    :param font_size:   font size for the header
    :return:
    """
    pdf.set_font(style="B", size=font_size)  # enabling bold text
    for col_format in formatting["columns"]:
        pdf.cell(
            w=col_format["width"],
            h=formatting["line_height"],
            txt=col_format["name"],
            border=1,
            ln=0,
            align="C",
        )
    pdf.ln(formatting["line_height"])
    pdf.set_font(style="")  # disabling bold text


def create_table(
    df: pd.DataFrame,
    line_height: int,
    column_width: list,
    output_filename: str,
    font_size: int = 8,
    orientation: str = "L",
):
    """
    Prints a table to pdf.

    Saves locally the table to be downloaded afterwards.
    :param df:              dataframe in list of lists format
    :param line_height:     Line height
    :param column_width:    Column width. Must be in the same order as df.columns
    :param output_filename  Filename for output pdf
    :param font_size:       font size for the table
    :param orientation:     orientation of paper: "L" for landspace, "P" for porttrait
    """
    # Set up pdf
    pdf = FPDF(orientation=orientation, format="A4")
    pdf.add_page()
    pdf.set_font("Times", "", size=font_size)

    # Create table formatting
    formatting = create_table_formtting(df, line_height, column_width)
    col_width = {
        col_format["name"]: col_format["width"] for col_format in formatting["columns"]
    }

    # Producing table
    render_table_header(pdf, formatting, font_size=font_size + 2)
    for row in df.values.tolist():
        if pdf.will_page_break(formatting["line_height"]):
            render_table_header(pdf, formatting, font_size=font_size + 2)
        for col_name, datum in zip(col_width.keys(), row):
            pdf.cell(
                w=col_width[col_name],
                h=formatting["line_height"],
                txt=str(datum),
                border=0,
                ln=0,
            )
        pdf.ln(formatting["line_height"])

    # Save temporary table
    os.makedirs(OUT_PDF_TABLES_PATH, exist_ok=True)
    pdf.output(output_filename)
