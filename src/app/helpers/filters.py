import streamlit as st


@st.experimental_memo
def create_default_df_filter(length):
    return [True for _ in range(0, length)]
