from datetime import datetime


def compute_age(birth_date):
    """
    Computes the age given the birth date.

    @param birth_date:  Birth date of all family component.
    @return:            Ages in years
    """
    return int(
        (datetime.now() - datetime.strptime(birth_date, "%d/%m/%Y")).days / 365.25
    )
