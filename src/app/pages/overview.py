from datetime import datetime

import streamlit as st

from app.data.loaders import load_anagrafica, load_products, load_tickets
from data.headers.anagraphic_header import AnagraphicHeader as ah
from data.headers.ticket_header import TicketsHeader as th
from app.helpers.time import compute_age


def app():

    st.title("Panoramica")

    anag = load_anagrafica(as_dataframe=True)
    products = load_products(as_dataframe=True)
    tickets = load_tickets(as_dataframe=True)

    right, middle, left = st.columns(3)

    # Anagrafica metrics
    users_count = anag.shape[0]
    families_count = anag[ah.family_id].unique().shape[0]
    mean_users_per_family = (
        anag.groupby(ah.family_id)[ah.family_id]
        .count()
        .reset_index(drop=True)
        .mean()
        .round(1)
    )
    mean_age = anag[ah.birth_date].apply(compute_age).mean().round(1)
    children_count = sum(anag[ah.birth_date].apply(compute_age) <= 14)

    right.markdown("### Anagrafica")
    right.metric("Totale utenti", users_count)
    right.metric("Totale famiglie ( = totale tessere)", families_count)
    right.metric("Componenti famiglia (media)", mean_users_per_family)
    right.metric("Età utenti (media)", mean_age)
    right.metric("Totale bambini (meno di 14 anni)", children_count)

    # Products metrics
    products_count = products.shape[0]
    best_product = (
        tickets.groupby(th.product)[th.quantity]
        .sum()
        .reset_index(drop=False)
        .sort_values(th.quantity, ascending=False)
        .head(1)
    )
    best_product_quantity = best_product[th.quantity].values[0]
    best_product = best_product[th.product].values[0]
    total_products_sold = int(tickets[th.quantity].sum())

    middle.markdown("### Prodotti")
    middle.metric("Numero prodotti a disposizione", products_count)
    middle.metric("Totale prodotti donati", total_products_sold)
    middle.metric(
        "Prodotto più donato (quantità)", f"{best_product} ({best_product_quantity})"
    )

    # Scontrini metrics
    min_ticket_date = datetime.strftime(tickets[th.date].min(), "%d/%m/%Y")
    max_ticket_date = datetime.strftime(tickets[th.date].max(), "%d/%m/%Y")
    tickets_count = tickets[th.ticket_id].unique().shape[0]
    mean_points = tickets.groupby(th.ticket_id)[th.total_points].sum().mean().round(1)
    mean_cloths = tickets.groupby(th.ticket_id)[th.total_points].count().mean().round(1)

    left.markdown("### Scontrini")
    left.metric(
        "Primo - Ultimo scontrino effettuato",
        str(min_ticket_date) + " - " + str(max_ticket_date),
    )
    left.metric("Totale scontrini effettuati", tickets_count)
    left.metric("Punti scontrino (media)", mean_points)
    left.metric("Numero di capi per scontrino (media)", mean_cloths)
