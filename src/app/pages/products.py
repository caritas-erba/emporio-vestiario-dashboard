import streamlit as st

from app.data.loaders import load_products
from app.helpers.beautifier import beautify_products_table
from app.helpers.filters import create_default_df_filter
from data.headers.products_header import ProductsHeader as ph
from app.components.download_buttons import download_buttons


def app():

    st.title("Prodotti")

    products = load_products(as_dataframe=True)

    default_filter = create_default_df_filter(products.shape[0])

    left, right = st.columns(2)
    selected_categories = left.multiselect(
        "Categoria", products[ph.category].sort_values(ascending=True).unique()
    )
    selected_targets = right.multiselect(
        "Destinatario",
        products[ph.target].sort_values(ascending=True).unique(),
    )
    if len(selected_targets) == 0 and len(selected_categories) == 0:
        df_filter = default_filter
    else:
        if len(selected_targets) > 0 and len(selected_categories) == 0:
            df_filter = products[ph.target].isin(selected_targets)
        elif len(selected_categories) > 0 and len(selected_targets) == 0:
            df_filter = products[ph.category].isin(selected_categories)
        else:
            df_filter = products[ph.target].isin(selected_targets) & products[
                ph.category
            ].isin(selected_categories)

    filtered_df = (
        products[df_filter].sort_values([ph.category, ph.target]).reset_index(drop=True)
    )

    st.dataframe(beautify_products_table(filtered_df), height=500)
    st.markdown(f"Stai visualizzando `{filtered_df.shape[0]}` prodotti.")

    download_buttons(
        products[ph.pdf_columns],
        filtered_df[ph.pdf_columns],
        ph.pdf_column_width,
        "prodotti.pdf",
        paper_orientation="P",
    )
