import pandas as pd
import numpy as np
import streamlit as st

from app.data.loaders import load_anagrafica, load_products, load_tickets
from data.headers.anagraphic_header import AnagraphicHeader as ah
from data.headers.ticket_header import TicketsHeader as th
from data.headers.products_header import ProductsHeader as ph
from app.helpers.time import compute_age
from app.components.figure import create_chart
from constants import PLOTLY_THEME, PLOTLY_FONT
from config import get_env, EnvironmentalVariableNames as EnvVar


@st.experimental_memo
def users_countries_barplot(anagraphic: pd.DataFrame):
    """
    Creates a barplot to count how may people come from each country.

    :param anagraphic:  dataframe with anagraphic data
    :return:            tuple (data for plotly, layout for plotly
    """
    anagraphic[ah.origin_country] = anagraphic[ah.origin_country].str.upper()
    country_data = (
        anagraphic.groupby(ah.origin_country)[ah.family_id]
        .count()
        .reset_index(drop=False)
        .rename(columns={ah.family_id: "count"})
        .sort_values("count", ascending=False)
    )
    country_data.loc[
        country_data[ah.origin_country] == "", ah.origin_country
    ] = "SCONOSCIUTO"

    hover = country_data[ah.origin_country] + ": " + country_data["count"].astype(str)
    plotly_data = [
        dict(
            type="bar",
            x=country_data[ah.origin_country],
            y=country_data["count"],
            hovertext=hover,
            hoverinfo="text",
            text=country_data["count"],
            textposition="auto",
        )
    ]
    layout = dict(
        title="Paesi di origine degli utenti",
        yaxis=dict(title="Numero di utenti"),
        font=PLOTLY_FONT,
        template=PLOTLY_THEME,
    )
    return plotly_data, layout


@st.experimental_memo
def users_age_barplot(anagrafica: pd.DataFrame):
    """
    Creates a barplot with the count of users by age.
    :param anagrafica:  dataframe with anagraphic data
    :return:            tuple (data for plotly, layout for plotly
    """
    anagrafica.loc[:, "age"] = anagrafica[ah.birth_date].apply(compute_age)
    count_df = (
        anagrafica.groupby(["age", ah.sex])[ah.birth_date]
        .count()
        .rename("count")
        .reset_index(drop=False)
    )

    sex_set = ["M", "F"]
    sex_labels = ["Maschi", "Femmine"]
    hover = {
        sex: f"Utenti {label} di "
        + count_df.loc[count_df[ah.sex] == sex, "age"].astype(str)
        + " anni: "
        + count_df.loc[count_df[ah.sex] == sex, "count"].astype(str)
        for sex, label in zip(sex_set, sex_labels)
    }
    plotly_data = [
        dict(
            type="bar",
            x=count_df.loc[count_df[ah.sex] == sex, "age"],
            y=count_df.loc[count_df[ah.sex] == sex, "count"],
            name=label,
            hovertext=hover[sex],
            hoverinfo="text",
        )
        for sex, label in zip(sex_set, sex_labels)
    ]
    layout = dict(
        title="Distribuzione di età degli utenti",
        xaxis=dict(title="Anni di età"),
        yaxis=dict(title="Numero di utenti"),
        font=PLOTLY_FONT,
        template=PLOTLY_THEME,
        barmode="stack",
    )
    return plotly_data, layout


@st.experimental_memo
def users_residence_barplot(anagrafica: pd.DataFrame):
    """
    Creates a barplot with number of users with residence in Erba vs all the others.

    :param anagrafica:  dataframe with anagraphic data
    :return:            tuple (data for plotly, layout for plotly
    """
    location = get_env(EnvVar.SILOE_EMPORIO_LOCATION).lower()
    location_colname = f"is_in{location}"
    anagrafica[ah.residence] = anagrafica[ah.residence].str.upper()
    anagrafica.loc[:, location_colname] = np.where(
        anagrafica[ah.residence].str.lower().str.contains(location),
        location.capitalize(),
        "Altro",
    )
    count_df = (
        anagrafica.groupby(location_colname)[ah.birth_date]
        .count()
        .rename("count")
        .reset_index(drop=False)
    )

    hover = (
        "Utenti residenti in "
        + count_df[location_colname]
        + ": "
        + count_df["count"].astype(str)
    )
    plotly_data = [
        dict(
            type="bar",
            y=count_df[location_colname],
            x=count_df["count"],
            hovertext=hover,
            hoverinfo="text",
            orientation="h",
            text=count_df["count"],
            textposition="auto",
        )
    ]
    layout = dict(
        title="Residenza degli utenti",
        yaxis=dict(title="Residenza"),
        xaxis=dict(title="Numero di utenti"),
        font=PLOTLY_FONT,
        template=PLOTLY_THEME,
    )
    return plotly_data, layout


@st.experimental_memo
def products_sells_barplot(tickets: pd.DataFrame, selected_products: list):
    """
    Creates a barplot with number of products sold.

    :param tickets: dataframe with tickets data
    :param head:    showing first head products
    :return:        tuple (data for plotly, layout for plotly
    """

    n_products = len(selected_products)
    tickets = (
        tickets[tickets[th.product].isin(selected_products)]
        if n_products != 0
        else tickets
    )

    count_df = (
        tickets.groupby(th.product)[th.quantity]
        .sum()
        .rename("count")
        .reset_index(drop=False)
        .sort_values(by="count", ascending=False)
    )

    if n_products == 0:
        count_df = count_df.head(20)
    count_df = count_df.sort_values(
        by="count", ascending=True
    )  # only for visualization purpouse

    hover = count_df[th.product] + ": " + count_df["count"].astype(str)
    plotly_data = [
        dict(
            type="bar",
            y=count_df[th.product],
            x=count_df["count"],
            hovertext=hover,
            hoverinfo="text",
            text=count_df["count"],
            textposition="auto",
            orientation="h",
        )
    ]
    subtitle = (
        "<br><sup>Mostrati i primi 20 capi più donati se nessun prodotto è stato selezionato</sup>"
        if n_products == 0
        else ""
    )
    layout = dict(
        title=f"Quantità donata di ogni prodotto{subtitle}",
        yaxis=dict(title="Prodotto"),
        xaxis=dict(title="Numero di capi donati"),
        font=PLOTLY_FONT,
        template=PLOTLY_THEME,
        height=800,
    )
    return plotly_data, layout


@st.experimental_memo
def products_sold_by_time(tickets: pd.DataFrame):
    """
    Creates a line plot to show how many products have been sold in a certain period of time
    :param tickets: dataframe with tickets data
    :return:        tuple (data for plotly, layout for plotly
    """
    count_df = (
        tickets.groupby(th.date)[th.quantity]
        .count()
        .rename("count")
        .reset_index(drop=False)
        .sort_values(by=th.date, ascending=True)
    )

    hover = (
        "Prodotti donati il "
        + count_df[th.date].astype(str)
        + ": "
        + count_df["count"].astype(str)
    )
    plotly_data = [
        dict(
            type="scatter",
            x=count_df[th.date],
            y=count_df["count"],
            hovertext=hover,
            hoverinfo="text",
            mode="markers+lines",
        )
    ]
    layout = dict(
        title="Quantità di prodotti donati nel tempo",
        xaxis=dict(title="Data"),
        yaxis=dict(title="Numero di capi donati"),
        xaxis_tickformat="%b-%Y",
        font=PLOTLY_FONT,
        template=PLOTLY_THEME,
    )
    return plotly_data, layout


def app():

    st.title("Grafici")

    anag = load_anagrafica(as_dataframe=True)
    products = load_products(as_dataframe=True)
    tickets = load_tickets(as_dataframe=True)

    st.markdown("## Anagrafica")
    data, layout = users_countries_barplot(anag)
    create_chart(data, layout)

    data, layout = users_age_barplot(anag)
    create_chart(data, layout)

    data, layout = users_residence_barplot(anag)
    create_chart(data, layout)

    st.markdown("## Prodotti")
    selected_products = st.multiselect(
        "Seleziona prodotti da visualizzare", products[ph.product]
    )
    # n_head = st.number_input("Numero di prodotti da visualizzare", min_value=20, step=1)
    data, layout = products_sells_barplot(tickets, selected_products)
    create_chart(data, layout)

    data, layout = products_sold_by_time(tickets)
    create_chart(data, layout)
