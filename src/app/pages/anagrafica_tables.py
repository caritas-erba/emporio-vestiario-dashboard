import streamlit as st

from app.data.loaders import load_anagrafica
from app.helpers.beautifier import beautify_anagrafica_table
from app.helpers.filters import create_default_df_filter
from data.headers.anagraphic_header import AnagraphicHeader as ah
from app.components.download_buttons import download_buttons


def app():

    st.title("Anagrafica")

    anag = load_anagrafica(as_dataframe=True)

    filter_method = st.radio(
        "Scegli metodo di selezione", ["Per id famiglia", "Per nome e cognome"]
    )

    default_filter = create_default_df_filter(anag.shape[0])
    if filter_method == "Per id famiglia":
        selected_ids = st.multiselect(
            "Seleziona l'id della famiglia",
            anag[ah.family_id].sort_values(ascending=True).unique(),
        )
        df_filter = (
            anag[ah.family_id].isin(selected_ids)
            if len(selected_ids) != 0
            else default_filter
        )
    else:
        left, right = st.columns(2)
        selected_surnames = left.multiselect(
            "Cognome", anag[ah.surname].sort_values(ascending=True).unique()
        )
        selected_names = right.multiselect(
            "Nome",
            anag.loc[anag[ah.surname].isin(selected_surnames), ah.name]
            .sort_values(ascending=True)
            .unique(),
        )
        if len(selected_names) == 0 and len(selected_surnames) == 0:
            df_filter = default_filter
        else:
            if len(selected_surnames) != 0 and len(selected_names) == 0:
                df_filter = anag[ah.surname].isin(selected_surnames)
            else:
                df_filter = anag[ah.name].isin(selected_names) & anag[ah.surname].isin(
                    selected_surnames
                )

    filtered_df = (
        anag[df_filter].sort_values(ah.family_id, ascending=True).reset_index(drop=True)
    )

    st.dataframe(beautify_anagrafica_table(filtered_df), height=500)
    st.markdown(f"Stai visualizzando `{filtered_df.shape[0]}` persone.")

    download_buttons(
        anag[ah.pdf_columns], filtered_df, ah.pdf_column_width, "anagrafica.pdf"
    )
