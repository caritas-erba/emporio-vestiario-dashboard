import streamlit as st

from app.data.loaders import load_cards
from app.helpers.beautifier import beautify_cards_table
from app.helpers.filters import create_default_df_filter
from data.headers.cards_header import CardsHeader as ch
from app.components.download_buttons import download_buttons


def app():

    st.title("Tessere")

    cards = load_cards(as_dataframe=True)

    filter_method = st.radio(
        "Scegli metodo di selezione", ["Per id tessera", "Per nome e cognome"]
    )

    default_filter = create_default_df_filter(cards.shape[0])
    if filter_method == "Per id tessera":
        selected_ids = st.multiselect(
            "Seleziona l'id della tessera",
            cards[ch.card_id].sort_values(ascending=True).unique(),
        )
        df_filter = (
            cards[ch.card_id].isin(selected_ids)
            if len(selected_ids) != 0
            else default_filter
        )
    else:
        left, right = st.columns(2)
        selected_surnames = left.multiselect(
            "Cognome", cards[ch.surname].sort_values(ascending=True).unique()
        )
        selected_names = right.multiselect(
            "Nome",
            cards.loc[cards[ch.surname].isin(selected_surnames), ch.name]
            .sort_values(ascending=True)
            .unique(),
        )
        if len(selected_names) == 0 and len(selected_surnames) == 0:
            df_filter = default_filter
        else:
            if len(selected_surnames) != 0 and len(selected_names) == 0:
                df_filter = cards[ch.surname].isin(selected_surnames)
            else:
                df_filter = cards[ch.name].isin(selected_names) & cards[
                    ch.surname
                ].isin(selected_surnames)

    filtered_df = (
        cards[df_filter].sort_values(ch.card_id, ascending=True).reset_index(drop=True)
    )

    st.dataframe(beautify_cards_table(filtered_df), height=500)
    st.markdown(f"Stai visualizzando `{filtered_df.shape[0]}` tessere.")

    download_buttons(
        cards[ch.pdf_columns], filtered_df, ch.pdf_column_width, "tessere.pdf"
    )
