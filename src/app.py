import streamlit as st

from app.multipages import MultiPage
from app.pages import anagrafica_tables, cards, overview, products, charts


st.set_page_config(
    page_title="Siloe Dashboard",
    page_icon=":tshirt:",
    layout="wide",
)

# Create an instance of the app
app = MultiPage()

# Add all your applications (pages) here
app.add_page("Panoramica", overview.app)
app.add_page("Anagrafica", anagrafica_tables.app)
app.add_page("Tessere", cards.app)
app.add_page("Prodotti", products.app)
app.add_page("Grafici", charts.app)

app.run(sidebar_title="Siloe Dashboard")
