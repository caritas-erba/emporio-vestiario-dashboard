import os

from config import EnvironmentalVariableNames as EnvVar, get_env

DB_FILE_PATH = get_env(EnvVar.SILOE_DB_FILE_PATH)

OUT_PDF_TABLES_PATH = os.path.join(get_env(EnvVar.SILOE_TEMP_PDF_DIR))

OUT_PDF_ALL_TABLE_FILE = os.path.join(
    OUT_PDF_TABLES_PATH,
    "all_table.pdf",
)
OUT_PDF_FILTERED_TABLE_FILE = os.path.join(
    OUT_PDF_TABLES_PATH,
    "filtered_table.pdf",
)

PLOTLY_THEME = "simple_white"
PLOTLY_FONT = dict(size=16)

DATE_DEFAULT_FORMAT = "%d/%m/%Y"
