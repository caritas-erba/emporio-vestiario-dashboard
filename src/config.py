import os


class EnvironmentalVariableNames:
    """Defines the names of the environmental variables used in the code and useful shortcuts"""

    # Environmental variables
    SILOE_DB_FILE_PATH = "SILOE_DB_FILE_PATH"
    SILOE_TEMP_PDF_DIR = "SILOE_TEMP_PDF_DIR"

    SILOE_EMPORIO_LOCATION = "SILOE_EMPORIO_LOCATION"


def get_env(env_var):
    """Returns the value of the environment variable env_var"""
    return os.environ[env_var]
