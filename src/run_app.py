import os
from subprocess import Popen, PIPE, STDOUT
import sys
import time
import webbrowser


def main():

    # Getting path to python executable (full path of deployed python on Windows)
    executable = sys.executable

    path_to_main = os.path.join(os.path.dirname(__file__), "app.py")

    # Running streamlit server in a subprocess and writing to log file
    proc = Popen(
        [
            executable,
            "-m",
            "streamlit",
            "run",
            path_to_main,
            "--server.headless=true",  # This appears to be necessary to start the server
            "--global.developmentMode=false",
        ],
        stdin=PIPE,
        stdout=PIPE,
        stderr=STDOUT,
        text=True,
    )
    proc.stdin.close()

    # Force the opening of the browser tab after a brief delay to let the the streamlit server start.
    # This is added since the deployed app does not seem able to automatically open the browser tab.
    time.sleep(3)
    webbrowser.open("http://localhost:8501")

    # The following, with the i/o configuration of Popen, should write to log file while program is running.
    # Actually does not do this.
    while True:
        s = proc.stdout.read()
        if not s:
            break
        print(s, end="")

    proc.wait()


if __name__ == "__main__":
    main()
