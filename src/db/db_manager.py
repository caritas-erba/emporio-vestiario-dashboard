import sqlite3
import logging
import os

import pandas as pd

from constants import (
    DB_FILE_PATH,
)


class DbManager:
    def __init__(self):
        pass

    @staticmethod
    def db_exists():
        return True if os.path.isfile(DB_FILE_PATH) else False

    @staticmethod
    def query(sql, to_df=False):
        """
        Execute the input sql string.

        @param sql:     Sql string to execute
        @param to_df:   Whether to convert the output to pandas dataframe
        @return:        List of tuples (corresponding to fetched rows). The list is empty if no data is found.
        """

        conn = None
        data = []
        try:
            conn = sqlite3.connect(DB_FILE_PATH)
            cur = conn.cursor()
            cur.execute(sql)
            data = cur.fetchall()
        except sqlite3.Error as e:
            logging.error(e)
            raise e
        finally:
            if conn:
                conn.close()
            if to_df:
                data = pd.DataFrame(data)
            return data
