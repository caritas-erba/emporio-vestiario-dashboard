class AnagraphicHeader:

    name = "nome"
    surname = "cognome"
    sex = "sesso"
    birth_date = "data_di_nascita"
    family_role = "ruolo_famigliare"
    residence = "residenza"
    citizenship = "cittadinanza"
    origin_country = "nazione_origine"
    mobile = "cellulare"
    email = "email"
    id_document_type = "tipo_documento_identita"
    id_document_number = "numero_documento_identita"
    family_id = "id_famiglia"

    ids = [name, surname, family_id]

    all = [
        name,
        surname,
        sex,
        birth_date,
        family_role,
        residence,
        citizenship,
        origin_country,
        mobile,
        email,
        id_document_type,
        id_document_number,
        family_id,
    ]
    beauty_all = [col.replace("_", " ").capitalize() for col in all]

    sex_set = ["M", "F"]
    family_role_set = ["Capofamiglia", "Moglie", "Marito", "Figlio", "Figlia"]
    id_document_type_set = [
        "Carta Identità",
        "Passaporto",
        "Permesso di Soggiorno",
        "Nessuno",
    ]

    pdf_columns = [
        name,
        surname,
        sex,
        birth_date,
        origin_country,
        mobile,
        family_id,
    ]
    pdf_column_width = [45, 45, 15, 40, 40, 50, 30]

    def __init__(self):
        pass
