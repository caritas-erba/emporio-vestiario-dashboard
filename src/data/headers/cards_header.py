class CardsHeader:

    card_id = "id_tessera"
    name = "nome"
    surname = "cognome"
    total_points = "punti_totali_periodo"
    current_points = "punti_attuali"
    service_start_date = "data_inizio_attivazione"
    service_end_date = "data_fine_attivazione"
    last_access_date = "data_ultimo_accesso"
    sent_by = "inviato_da"
    motivation_sent = "motivazione_invio"
    other_caritas_services = "altri_servizi_caritas"

    ids = [card_id, name, surname]

    all = [
        card_id,
        name,
        surname,
        total_points,
        current_points,
        service_start_date,
        service_end_date,
        last_access_date,
        sent_by,
        motivation_sent,
        other_caritas_services,
    ]
    beauty_all = [col.replace("_", " ").capitalize() for col in all]

    sent_by_set = [
        "Caritas Erba",
        "Centro ascolto",
        "Parrocchia Erba",
        "Servizi sociali",
    ]

    motivation_sent_set = [
        "Difficoltà economiche",
        "Famiglia numerosa",
        "Disoccupazione",
        "Altro",
    ]

    other_caritas_services_set = [
        "Emporio Alimentare",
        "Progetto Decima",
        "Emergenza Abitativa",
        "CAS",
        "Altro",
    ]

    pdf_columns = [
        card_id,
        name,
        surname,
        total_points,
        current_points,
        service_end_date,
        last_access_date,
    ]
    pdf_column_width = [22, 45, 45, 45, 30, 50, 46]
