class ProductsHeader:

    product = "prodotto"
    category = "categoria"
    target = "destinatario"
    description = "descrizione"
    points = "punti"
    max_purchasable = "max_acquistabili"

    ids = [product, category, target]

    all = [product, category, target, description, points, max_purchasable]
    beauty_all = [col.replace("_", " ").capitalize() for col in all]

    categories_set = [
        "Abiti",
        "Intimo",
        "Notte",
        "Scarpe",
        "Bagno",
        "Biancheria",
        "Accessori",
        "Neonato",
    ]
    target_set = ["Uomo-Donna-Ragazzo-Ragazza", "Bambino-Bambina", "Neonato", "Casa"]
    target_short_set = ["Adulto", "Bambino", "Neonato", "Casa"]

    pdf_columns = [
        product,
        category,
        target,
        points,
    ]
    pdf_column_width = [60, 40, 60, 20]

    @staticmethod
    def map_target_to_target_short():
        return {
            ProductsHeader.target_set[i]: ProductsHeader.target_short_set[i]
            for i in range(0, len(ProductsHeader.target_set))
        }

    def __init__(self):
        pass
