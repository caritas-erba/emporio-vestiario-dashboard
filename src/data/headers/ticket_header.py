class TicketsHeader:

    ticket_id = "id_scontrino"
    card_id = "id_scheda"
    product = "prodotto"
    product_category = "categoria_prodotto"
    product_target = "destinatario_prodotto"
    quantity = "quantita"
    unit_points = "punti_unitari"
    total_points = "punti_totali"
    date = "data"

    all = [
        ticket_id,
        card_id,
        product,
        product_category,
        product_target,
        quantity,
        unit_points,
        total_points,
        date,
    ]

    def __init__(self):
        pass
