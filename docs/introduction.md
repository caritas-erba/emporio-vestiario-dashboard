# Introduzione
Siloe è una dashboard che permette la visualizzazione e l'esplorazione dei dati raccolti tramite il software [Zaccheo](https://gitlab.com/caritas-erba/zaccheo-crm).

Siloe permette di:
- visualizzare e scaricare liste complete o filtrate dei dati di:
  - anagrafica degli utenti;
  - tessere;
  - prodotti;
- visualizzare alcuni grafici riassuntivi dei dati;
- visualizzare alcune statistiche riassuntive dei dati.

## Applicazione
Siloe è costituito da un'interfaccia grafica integrata nel browser: all'avvio dell'applicazione, si aprirà in automatico una nuova tab nel browser di default nel sistema.

Se l'applicazione non compare dopo pochi secondi dall'apertura della tab, oppure viene visualizzato un messaggio di errore (qualcosa di simile a "Impossibile connettersi"), eseguire il refresh della pagina finché l'applicazione non viene caricata correttamente.

L'interfaccia è composta di 5 pagine, navigabili tramite il menù situato nella barra a sinistra della schermata:
- **panoramica**: espone alcune statistiche riassuntive dei dati;
- **anagrafica**: per esplorare i dati di anagrafica registrati da Zaccheo;
- **tessere**: per esplorare i dati delle tessere registrati da Zaccheo;
- **prodotti**: per esplorare il catalogo prodotti registrato in Zaccheo;
- **grafici**: contiene alcuni grafici interattivi riassuntivi dei dati.

I dati mostrati dalla dashboard sono aggiornati automaticamente ad ogni nuovo ingresso, in quanto provengono direttamente dal database sottostante a Zaccheo.
