# Pagine

## Panoramica

Nella pagina di panoramica sono disponibili alcune statistiche riassuntive dei dati relativi all'Emporio.

## Tabelle

Sono presente 3 pagine che mostrano le informazioni principali relative a:
- anagrafica;
- prodotti;
- tessere.

In ogni pagina è possibile filtrare le righe visualizzate selezionando gli utenti o i prodotti di interesse.
È inoltre possibile scaricare le tabelle complete oppure filtrate.

## Grafici

In questa pagina sono visualizzati alcuni grafici dei dati.

Ogni grafico è interattivo: spostando il mouse su una linea o barra saranno visualizzate informazioni aggiuntive, ed è possibile selezionare o deselezionare le diverse tracce cliccando sulle voci della legenda, dove presente.

In alcuni grafici sono presenti dei filtri che permettono di selezionare e visualizzare solo un sottoinsieme degli elementi disponibili.
