# Changelog

<!--next-version-placeholder-->

## v1.0.5 (2022-07-19)
### Fix
* Removed run_app script ([`c3cab1d`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/c3cab1d5381cd467e3cced28943761f9dca1ba51))

### Documentation
* **readme:** Added note on versioning ([`9d6e075`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/9d6e07591e640fa0c72e98d06b6999661a6902af))
* Suggestion to refresh page when loading app ([`9c9e38b`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/9c9e38b5da73ef2b551a5b531f2b764e6f35838a))

## v1.0.4 (2022-07-19)
### Fix
* Fixed syntax ([`5203c95`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/5203c95312a940a0e2a3623dc24cca4449c52e97))

## v1.0.3 (2022-07-19)
### Fix
* Updated version and added missing package in installer.cfg ([`a43eca1`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/a43eca1d9346fcfd66f4501373dae1ea9b1953f6))

## v1.0.2 (2022-07-19)
### Fix
* Added comment ([`14c74ba`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/14c74ba3d51326a26398793ff0a7c574a92aca91))

## v1.0.1 (2022-07-19)
### Fix
* **ci:** Fixed asset links for release ([`ab1c2fd`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/ab1c2fd4ec54baf33eb962b7f34e1f59d15b4e1b))

## v1.0.0 (2022-04-25)
### Feature
* **charts:** Generalized residence chart through env var ([`98d211f`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/98d211f3cd7d0079e8c2c51102e517e259ebcb8b))
* **charts:** Possibility to choose which product visualize in the sold product number chart ([`5543e18`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/5543e182ca1d67da9c554e5a2bac55346add3bca))
* Added brief delay before forcing the opening of the tab ([`0870e5b`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/0870e5bb154e69087f3105ecb27831e55d24e0d6))
* **charts:** Added division by sex in plot by age ([`be15d06`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/be15d068d2b9b0b5656ce33d2f61e3bc01a404b0))
* **charts:** Added page with charts ([`a2e8bf8`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/a2e8bf866686c2918e303ae876812341ec7ebb61))
* **overview:** Finished with age computation ([`8200bfb`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/8200bfb8f33bd903e8bac58fd0cc3b6ecfa3d69f))
* **overview:** Added more metrics ([`683a395`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/683a3958a611d54d7aa3f195a0c5287093c98059))
* Added some metrics in overview ([`a2c75e3`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/a2c75e3a74163555cd290cbc92f21e22757e81ce))
* **download_buttons:** Added parameter for paper orientation ([`8a9d556`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/8a9d556e28f42679e63fcda2133ef5dff96147ee))
* **products:** Added page ([`d67eec8`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/d67eec89a3a4bd375a0e892508c2f6429c63df22))
* **app:** Customized browser tab title and icon ([`882cb4b`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/882cb4bdb68216130cbcc5fd05937e85f9106d88))
* **overview:** Added skeleton ([`3d432c4`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/3d432c4df3e9436801e3684150f8e10ddf9b0384))
* **cards:** Added cards page ([`0274a05`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/0274a0520770b3f0999a93da1119f45260d6e813))
* **pages:** One page for tables and one for charts for anagrafica ([`334b1cf`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/334b1cfdca89f7cc27e6ee9d0914698dcb6e3f24))
* **pdf:** Added helpers to generate pdf and download buttons ([`bc6610f`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/bc6610f3a99c04e3ef6b6a485d2d37a5920bcb44))
* **anagrafica:** Added filter and first graph ([`e24d70f`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/e24d70f62a442dbb2cc8414ed54e697a6e4c4834))
* Added anagrafica page and first trials for downloading pdf ([`a771bfd`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/a771bfdae1027d42a3fb8273d61059335e48c940))
* **run_app:** Waiting some seconds before opening the browser tab ([`3836645`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/3836645dc8e238f38bf23209038517a6140f2c0d))
* Added plotly ([`e276675`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/e2766752b310d52e4cc7ab28a85652cf520d5651))

### Fix
* **charts:** Fixed chart on product sold ([`8641648`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/864164849388f52c235a9ebd355bb18b58c9473c))
* **overview:** Fixed min max dates of tickets and added total products sold ([`e128c1b`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/e128c1b02eadc30bd36e70ad4c8d2a742fead4b0))
* **overview:** Fixed some metrics and removed cards metrics ([`dc27bc9`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/dc27bc9284d5bef8aaa38b2ad33788ee5873a102))
* **overview:** Correct aggregation to count product ([`aea7d08`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/aea7d089da5b8fc9e51f0a61cb60ca17161d1def))
* **charts:** Fixed some visualization problems" ([`4c79c95`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/4c79c9577c849584d962a4bdb6b5aa35d4deac3a))
* Setting right path to output pdf folder ([`4712842`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/4712842a9885859e35566e05a2a5befb4b59ed43))
* **pages:** Correct column filtering in dowload of filtered table ([`00b0b34`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/00b0b3419557969824eab7def2df8772f4df96a1))
* **cards:** Removed sample code for problem with st.dataframe ([`c2ac0ba`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/c2ac0ba80aab3593acffb979d720602897afcf35))
* Added correct env var for working directory ([`325b6d8`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/325b6d8ca09c4e802a96524b622563967c0f4779))
* Typo in copy directive ([`6658356`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/66583563fa26bb40fa007948c0f77b8a849779cb))
* Corrected to make it work on windows ([`c2c62f5`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/c2c62f502cd56c7b11cb2cf37fce64f8e5eb105d))

### Breaking
* want to update to version 1.0.0 ([`169ed41`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/169ed4132dacd6ca925e27d9e675a937c9f6831c))

### Documentation
* Completed ([`fa6c174`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/fa6c1747e06c69127e4932e3705f5835f5ad4b58))
* Added skeleton and introduction ([`35c5369`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/35c5369862e55db5e9cf74c89fe87922eccdeb54))
* Updated readme ([`96c22d5`](https://gitlab.com/caritas-erba/emporio-vestiario-dashboard/-/commit/96c22d5dd59dcf1aa15227fbb73e439ba7002414))
